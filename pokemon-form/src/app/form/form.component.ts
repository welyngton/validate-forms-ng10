import { Component, OnInit, forwardRef } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, NG_VALUE_ACCESSOR} from '@angular/forms';
import { Ipokemon } from './form.models'


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {

  pokemonForm: FormGroup;


  type = new FormControl();
  typeList: string[] = ['Normal', 'Fighting', 'Flying', 'Poison', 'Ground',
  'Rock', 'Bug', 'Ghost', 'Steel', 'Fire', 'Water', 'Grass', 'Eletric', 'Psychic', 'Ice',
  'Dragon', 'Fairy', 'Dark'];


  pokemonGenders = [
    "Male",
    "Female"
  ];


  validation_messages = {
    'pokemonName': [
      { type: 'required', message: 'Full name is required' }
    ],
    'pokemonGender': [
      { type: 'required', message: 'Please select your gender' },
    ],
 
    'pokemonType': [
      { type: 'required', message: 'Please select your pokemon type' },
    ],
    'pokemonBirthday': [
      { type: 'required', message: 'Please insert your birthday' },
    ],
    };


  options: FormGroup;
  colorControl = new FormControl('primary');
  fontSizeControl = new FormControl(16, Validators.min(10));

  constructor(private fb: FormBuilder) {
    
  }
  ngOnInit(): void {
    
    this.options = this.fb.group({
      color: this.colorControl,
      fontSize: this.fontSizeControl,
    });
    
    
    this.pokemonForm = this.fb.group({
      pokemonName: ['', Validators.required],
      pokemonGender: new FormControl(this.pokemonGenders[0], Validators.required),
      pokemonBirthday: ['', Validators.required],


    })

  }

  getFontSize() {
    return Math.max(10, this.fontSizeControl.value);
  }


}
